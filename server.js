var http = require("http");
var fs = require("fs");
var path = require("path");
var mime = require("mime");
var cach = {};

var server = http.createServer(function (req,res) {
    var filePath=false;
    if(req.url=="/"){
        filePath="public/index.html";
    }else {
        filePath="public"+req.url;
    }
    var absPath="./"+filePath;
    serverStatic(res,cach,absPath);
});
server.listen(3000,function () {
    console.log("localhost:3000");
});

function send404(res) {
    res.writeHead(404,{"Content-Type":"text/html"});
    res.end("<h1>404页面没找到</h1>");
}
function sendFile(res,filePath,fileContents) {
    res.writeHead(200,{"Content-Type":mime.lookup(path.basename(filePath))});
    res.end(fileContents);
}
function serverStatic(res,cache,absPath) {
    if(cache[absPath]){
        sendFile(res,absPath,cache[absPath]);
    }else {
        fs.exists(absPath,function (exits) {
            if(exits){
                fs.readFile(absPath,function (err,data) {
                    if(err){
                        send404(res);
                    }else {
                        cache[absPath]=data;
                        sendFile(res,absPath,data);
                    }
                })
            }else {
                send404(res);
            }
        })
    }
}